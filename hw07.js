function submenuOpener(elem) {
    elem.classList.toggle('submenu_opened');
}

const navigationWrapperBlock = document.querySelectorAll('.submenu');

navigationWrapperBlock.forEach(elem => {
    if (elem.nextSibling != null) {
        elem.addEventListener('click', () => submenuOpener(elem));
    }
});