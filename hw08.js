function contentLineMaker() {
    const allLectures = document.querySelectorAll('.container');
    const contentLineBlock = document.querySelector('.content-line-block');
    const lecturesCount = document.querySelector('.breadcrumbs-block__button_count');
    if (allLectures.length < 5 && allLectures.length > 1) {
        lecturesCount.textContent = `${allLectures.length} лекции`
    } else if (allLectures.length === 1) {
        lecturesCount.textContent = `${allLectures.length} лекция`
    } else {
        lecturesCount.textContent = `${allLectures.length} лекций`
    }
    allLectures.forEach(elem => {
        let newContainer = elem.cloneNode(true);
        contentLineBlock.append(newContainer);
    });

    for (let i = 0; i < contentLineBlock.children.length; i++) {
        if (i > 3) {
            contentLineBlock.children[i].classList.add('container_hidden');  //временное решение проблемы, пока нет слайдера
        }
    }
}

function filter(filterParameter) {
    const containers = document.querySelector('.content-line-block').children;
    const children = [...containers];
    filterParameter.classList.toggle('breadcrumbs-block__button_checked');

    children.forEach(child => {
        child.classList.remove('container_hidden');
        let filterContainerText = child.querySelector('.container__title').textContent.trim();

        if (
            filterContainerText !== filterParameter.textContent.trim().toUpperCase() &&
            filterParameter.id !== 'allLectures'
        ) {
            child.classList.add('container_hidden');
        }
    });
}

/*function slider(direction) {
    const contentLineBlock = document.querySelectorAll('.content-line-block');
    const sliderArray = [];
    contentLineBlock.forEach(elem => sliderArray.push(elem.children));
    console.log(sliderArray);
    if (direction === 'left') {

    } else {
        console.log('j')
    }
}*/

document.addEventListener('DOMContentLoaded', contentLineMaker);

const buttonWrapper = document.querySelector('.breadcrumbs-block');
buttonWrapper.addEventListener('click', e => filter(e.target));
