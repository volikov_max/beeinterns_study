function objectCreator() {
    const allLectures = document.querySelectorAll('.container');
    const lecturesObj = {};
    let elemGroup;
    allLectures.forEach(elem => {
        elemGroup = elem.dataset.group;
        lecturesObj[elemGroup] = [];
    });
    for (let i = 0; i < allLectures.length; i++) {
        const containerObj = {
            "title": allLectures[i].querySelector('.container__title').textContent.trim(),
            "description": allLectures[i].querySelector('.container__subtitle').textContent.trim(),
            "date": allLectures[i].querySelector('.container-wrapper__text').textContent.trim(),
            "image": allLectures[i].querySelector('.container__picture').src,
            "label": allLectures[i].querySelector('.container__sticker').textContent.trim(),
        };
        elemGroup = allLectures[i].dataset.group;
        if (elemGroup in lecturesObj) {
            lecturesObj[elemGroup].push(containerObj);
        }
    }
    console.log(lecturesObj);
}

objectCreator();