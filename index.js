const containerTitle = document.querySelectorAll('.container__title');
for (let i = 0; i < 4; i++) {
    containerTitle[i].textContent = containerTitle[i].textContent.toUpperCase();
}

const containerSubtitle = document.querySelectorAll('.container__subtitle');
for (let i = 0; i < 4; i++) {
    let text = containerSubtitle[i].textContent.trim();
    if (text.length >= 20) {
        text = text.slice(0, 20);
        containerSubtitle[i].textContent = text + '...';
    }
}
